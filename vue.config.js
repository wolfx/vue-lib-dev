// vue.config.js
module.exports = {
  lintOnSave: false,
  parallel: 4,
  css: {
    extract: false
  },
  configureWebpack: {
    externals: {
      "element-ui": "element-ui"
    }
  }
};
